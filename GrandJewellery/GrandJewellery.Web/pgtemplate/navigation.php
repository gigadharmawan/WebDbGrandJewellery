﻿<head>
	<link href="assets/Navbar.css" rel="stylesheet" type="text/css"></link>
</head>
<?php
	session_start();
	if(!isset($_SESSION['admin'])){
		$_SESSION['admin'] = "NonMember";
	}
?>
<div class="topMenu">
	<a href="index.php">Home</a>
	<a href="product.php">Product</a>
	<?php
		if(isset($_SESSION['email'])){
	?>
		<a href="profile.php">Profile</a>
		<a href="doLogout.php">Logout</a>	
		<a href="transaction.php">Transaction</a>
	<?php
			if(isset($_SESSION['email']) && $_SESSION['admin'] != "Admin"){
	?>
				<a href="cart.php">Cart</a>
	<?php
			}
		}
		else{
	?>
			<a href="login.php">Login</a>
			<a href="register.php">Register</a>
			
	<?php	
		}
	?>
		<a href="testimonial.php">Testimonial</a>
</div>