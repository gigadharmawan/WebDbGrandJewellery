﻿<html>
<head>
    <title>Grand Jewellery</title>
	<link href="assets/Global.css" rel="stylesheet" type="text/css"></link>
</head>
<body>	
	<div class="container">
		<?php include '/pgtemplate/header.php'; ?>

		<?php include '/pgtemplate/navigation.php'; ?>
		<div class="wrapper">
			<div class="content">
			<h2>Register</h2>
			<form action="doRegister.php" method="post">
						<label>Email</label>
						<input type="text" name="email" />
						<br />
						<label>Name</label>
						<input type="text" name="name" />
						<br />
						<label>Date of Birth</label>
						<input type="number" min="1" max="30" name="day" placeholder="DD"/>
						<input type="number" min="1" max="12" name="month" placeholder="MM"/>
						<input type="number" min="1990" name="year" placeholder="YYYY"/>
						<br />
						<label>Gender</label>
						<input type="radio" name="gender" value="Male"/>Male
						<input type="radio" name="gender" value="Female"/>Female
						<br />
						<label>Password</label>
						<input type="password" name="pass" />
						<br />
						Yes, I do want to register in this site
						<input type="checkbox" name="agree" />
						<br />
						<input type="submit" value="Register" />
					</form>
					<div class="errorMsg">
				<?php
					if(isset($_REQUEST['msg'])){
						$case = $_REQUEST['msg'];
						switch($case){
							case 1: echo "Please fill all the required field"; break;
							case 2: echo "You must agree to join our site"; break;
							case 3: echo "Wrong email format"; break;
							case 4: echo "Name length must between 7 & 15 characters"; break;
							case 5: echo "Wrong date format"; break;
							case 6: echo "Register failed"; break;
						}
					}
				?>
			</div>
			</div>
		</div>
	<!--Insert pgtemplate here-->

	</div>

</body>
</html>