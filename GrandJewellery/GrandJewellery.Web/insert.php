<?php
		include 'connect.php';

			$newid = "";
			$query = "SELECT * FROM jewelry";
			$data = mysql_query($query);
			
			if($data === FALSE) { 
				die(mysql_error()); // TODO: better error handling
			}

			if($row = mysql_fetch_array($data)){
				$lastid = (int) substr($row['JewelID'], 2, 3)+1;
	
				if($lastid < 10){
					$newid = "PR00" . $lastid;
				}
				else if($lastid < 100){
					$newid = "PR0" . $lastid;
				}
				else{
					$newid = "PR" . $lastid;
				}
			}
			else{
				$newid = "PR001";
			}
		
		?>
<html>
<head>
	<link href="assets/Global.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
	<div class ="container">
	
	<?php include '/pgtemplate/header.php'; ?>

	<?php
		include '/pgtemplate/navigation.php';
	?>

		<div class="wrapper">
			<div class = "content">
            <h2>Jewelry</h2>
            <form class = "insert" action="doInsert.php" method="post" enctype="multipart/form-data">
                            <label>Jewel ID</label>
                            <input type="text" name="jewelID" id="jewelID" value="<?=$newid?>" readonly/>
							<br />
                            <label>Name</label>
                            <input type="text" name="name" id="name" />
							<br />
                            <label>Description</label>
                            <input type="text" name="description" id="description" />
							<br />
                            <label>Price</label>
                            <input type="text" name="price" id="price" />
							<br />
                            <label>Stock</label>
                            <input type="text" name="stock" id="stock" />
							<br />
                            <label>Photo</label>
							<div><input type="file" name="picture"/></div>
							<br />
				<div class="tombol">
					<button class = "button" type="submit" name="insert" id="insert"><b>Insert</b></button>
				</div>
            <a href="product.php">Product List</a>
            </form>
			<div class="errorMsg">
    <?php
		if(isset($_REQUEST['err'])){
		$case = $_REQUEST['err'];
						switch($case){
							case 1: echo "Nama harus diisi"; break;
							case 2: echo "Nama harus diakhiri dengan Jewel"; break;
							case 3: echo "Stock harus diisi"; break;
							case 4: echo "Stock harus angka"; break;
							case 5: echo "Stock harus lebih dari 0"; break;
							case 6: echo "Price harus diisi"; break;
							case 7: echo "Price harus angka"; break;
							case 8: echo "Price harus diantara 100000 - 1000000"; break;
							case 9: echo "Gambar harus .jpg atau .png"; break;
						}
					}
				?>
	</div>
	</div>
	</div>
	</div>
</body>
</html>