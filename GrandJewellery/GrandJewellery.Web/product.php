﻿<html>
<head>
    <title>Grand Jewellery - Product</title>
	<link href="assets/Global.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<?php
	include 'connect.php';
?>
	<div class ="container">
	
	<?php include '/pgtemplate/header.php'; ?>

	<?php
		include '/pgtemplate/navigation.php';
	?>

		<div class="wrapper">
			<div class = "content">
				<h2>Products</h2>
				<div>
					<form method="get">
						<label>Search: </label>
						<input type="text" placeholder="Search by name...." name="search" />
						<input type="submit" value="Search" />
					</form>
					<?php
						$cari="";
						$search = "";
						if(isset($_GET['search'])){
							$cari = $_GET['search'];
							$search = "WHERE `Name` LIKE '%".$cari."%'";
						}
						
						$currpage = 1;
						if(isset($_GET['curr'])){
							$currpage = $_GET['curr'];
						}
						$dpp = 4;
						$start = ($currpage-1)*$dpp;
						
						$query = "SELECT * FROM `Jewelry` $search LIMIT $start, $dpp";
						$result = mysql_query($query);
						while($row = mysql_fetch_array($result)){
					?>
					<br />
					<div class="productTable">
						<img src="<?=$row[5]?>" width="100px" height="100px"/>
						<label>ID: <?=$row[0]?></label>
						<br />
						<label>Name: <?=$row[1]?></label>
						<br />
						<label>Description: </label>
						<br />
						<label><?=$row[4]?></label>
						<br/ >
						<label>Stock: <?=$row[2]?></label>
						<br />
						<label>Price: <?=$row[3]?></label>
						<?php
							if($_SESSION['admin'] == "Admin"){
						?>
							<div style="float: right; width: 200px; display: flex;">
							<form style="width: auto;" action="update.php?id=<?=$row[0]?>" >
								<input type="hidden" name="id" value=<?=$row[0]?> />
								<button type="submit">
									<a class="formButton" href="update.php?id=<?=$row[0]?>">Update</a>
								</button>
							</form>
								<form style="width: auto;" action="doDeleteProduct.php" method="post" style="float: right;">
									<input type="hidden" name="id" value=<?=$row[0]?> />
									<button type="submit" name="password">
										<a class="formButton" href="doDeleteProduct.php">Delete</a>
									</button>
								</form>
							</div>
						<?php
							}
						?>

						<?php
						if(isset($_SESSION['email']) && $_SESSION['admin'] != "Admin"){
						?>
							<form method="post" style="float: right;" action="doUpdateCart.php">
								<label>Qty: </label>								
								<input type="hidden" value="<?=$row[0]?>" name ="txtID"/>
								<input type="hidden" value="<?=$row[1]?>" name ="txtName"/>
								<input type="number" min="0" max="<?=$row[2]?>" name ="txtQty"/>
								<input type="hidden" value="<?=$row[3]?>" name ="txtPrice"/>
								<input type="hidden" value="<?=$row[2]?>" name ="txtAmount"/>
								<input type="submit" value="Add to Cart" />
							</form>
						<?php
						}
						?>
						
					</div>
					<?php
						}
					?>
				<div>
			</div>		
		</div>
		<?php
					$query = "SELECT * FROM `Jewelry` $search";
					$data = mysql_query($query);
					$totaldata = mysql_num_rows($data);
					$totalpage = $totaldata/$dpp;
					
					for($i = 1; $i<$totalpage + 1; $i++){
				?>	
					<a class="formButton" style="margin-top: 10px; margin-bottom: 10px" href="product.php?search=<?=$cari?>&curr=<?=$i?>"><?=$i?> </a>
				<?php
					}
					
				?>

				<?php
						if($_SESSION['admin'] == "Admin"){
						?>
							<form action="insert.php" method="post">
								<button type="submit" name="password" id="changePass">
									<a class="formButton" href="changePass.php"><b>Insert New Item</b></a>
								</button>
							</form>
						<?php
						}
						?>
		<?php
			require '/pgtemplate/footer.php';
		?>
	</div>
</body>
</html>