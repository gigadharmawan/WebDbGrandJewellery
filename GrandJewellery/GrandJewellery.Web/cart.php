﻿<html>
<head>
    <title>Grand Jewellery</title>
</head>
<body>
	<div class = "container">		
		
	<?php
	 //DETERMINE SESSION
	include '/pgtemplate/header.php';
	include '/pgtemplate/navigation.php';
	if($_SESSION['admin']!="Member")
	{header('location:login.php?error=1');}
	else
	{
	?> 
		<div class = "wrapper">
			<div class = "content">
				<?php
					if(isset($_SESSION['cart']) && $_SESSION['cart']!=array())
					{
				?>			
			<h2>Cart</h2>
			<table border="1px solid" style="margin: 0 auto;">
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th>Option</th>
				</tr>
				<?php
					$count=0;
					foreach($_SESSION['cart'] as $data )
					{
				?>				
					<tr>
					<td><?=$data['0']?></td>
					<td><?=$data['1']?></td>
					<td><?=$data['2']?></td>					
					<td><?=$data['3']?></td> 
					<td><?=$data['2']*$data['3']?></td>
					<td><a class="formButton" href = "doCartRemove.php?id=<?=$data['0']?>&qty=<?=$data['4']?>&cnt=<?=$count?>">Remove</a></td>	
					</tr>
					<?php 
					$count++; 
					}
					?>
			</table>
			<br />
			<br />
			<br />
			<a href = "doCheckOut.php" class = "button fright"> Check Out</a><br/>
			<?php
					}else{ "<h3> No Data Found</h3>";}
			?>											
			<br />
			<br />
			</div>
		</div>
		
	
	<?php	
	}//end else
	include '/pgtemplate/footer.php';
	?>
	</div>
</body>
</html>